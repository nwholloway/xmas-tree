#! /usr/bin/python3

import random
import sys
from time import sleep

from gpiozero import LEDBoard


def pulse(tree):
    tree.pulse(fade_in_time=2, fade_out_time=1, n=10, background=False)


def chase(tree):
    for n in range(len(tree.leds), 0, -1):
        for i in range(0, n - 1):
            led1, led2 = tree.leds[i], tree.leds[i + 1]
            led1.on()
            sleep(0.1)
            led2.on()
            led1.off()
            sleep(0.1)
    tree.on(0)
    sleep(5)


def twinkle(tree):
    leds = list(tree.leds)
    random.shuffle(leds)

    for led in leds:
        led.blink(on_time=random.uniform(0.2, 0.3), off_time=random.uniform(0.75, 1.25))
        sleep(3)
    sleep(30)


def static(tree):
    tree.on()
    sleep(15)


def redgreen(tree):
    def setleds(leds, value):
        for led in leds:
            led.value = value

    star = [tree.star]
    red = [tree.top.left, tree.middle.left, tree.middle.midright, tree.bottom.midleft, tree.bottom.right]
    green = [tree.top.right, tree.middle.midleft, tree.middle.right, tree.bottom.left, tree.bottom.midright]

    for i in range(0, 10):
        setleds(red, True)
        sleep(1)
        setleds(red, False)
        setleds(green, True)
        sleep(1)
        setleds(green, False)
        setleds(star, True)
        sleep(1)
        setleds(star, False)


def main():
    print('Xmas Tree starting')
    print('https://gitlab.com/nwholloway/xmas-tree')

    tree = LEDBoard(
        pwm=True,
        star=12,
        top=LEDBoard(
            pwm=True,
            left=23, right=24,
            _order=('left', 'right')
        ),
        middle=LEDBoard(
            pwm=True,
            left=19, midleft=20, midright=21, right=22,
            _order=('left', 'midleft', 'midright', 'right')
        ),
        bottom=LEDBoard(
            pwm=True,
            left=11, midleft=16, midright=17, right=18,
            _order=('left', 'midleft', 'midright', 'right')
        ),
        _order=('bottom', 'middle', 'top', 'star')
    )

    effects = [chase, pulse, twinkle, redgreen, static]

    while True:
        effect = random.choice(effects)
        # print(effect.__name__)
        effect(tree)

        tree.off()
        sleep(5)


if __name__ == '__main__':
    sys.tracebacklimit = 1
    main()
