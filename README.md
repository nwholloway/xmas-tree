# Christmas Tree

This is my code for the ["Christmas Tree Programmable
Kit"](https://thepihut.com/products/christmas-tree-programmable-kit)
from [@ThePiHut](https://twitter.com/ThePiHut). 

This was an impulse purchase in 2019, and I downloaded the [sample
code](https://github.com/modmypi/Programmable-Xmas-Tree/) onto a handy Raspberry Pi 3A+,
quickly modified to my own liking, and had it running on my desk.
  
The code was edited directly on the Pi, and I appear to have not made a copy in a safe place.

So that I don't have to repeat the phrase, _"I've lost the code to my Christmas
tree"_, I've created this [repository](https://gitlab.com/nwholloway/xmas-tree)
and added [Ansible](https://www.ansible.com/) deployment.

# Installation

The script is run as a `systemd` service, so it will start automatically when the Pi boots.
The service runs as user `nobody` and group `gpio`.

## Manual Deployment

This is the easiest way to install once you have the files `xmas-tree.py` and
`xmas-tree.service` on your Pi.

The initial installation is performed using these commands:
```
sudo apt install -y python3-gpiozero
sudo install -d /usr/local/lib/xmas-tree
sudo install -t /usr/local/lib/xmas-tree xmas-tree.py
sudo install -t /etc/systemd/system -m 0644 xmas-tree.service
sudo systemctl daemon_reload
sudo systemctl enable xmas-tree
sudo systemctl start xmas-tree
```

To check on the status of the service (and any errors):
```
systemctl status xmas-tree
```

To update the installed script:
```
sudo install -t /usr/local/lib/xmas-tree xmas-tree.py
sudo systemctl restart xmas-tree
```

## Using Ansible

I use Ansible to automate the deployment to a remote Pi.  The Pi is configured for
SSH access, and the remote user is able to `sudo` without needing to provide a password.

Update `inventory.ini` and change the hostname to be your Raspberry Pi, and possibly
uncomment the `ansible_ssh_user` variable.

Initial deployment and updates are simply performed using:
```
ansible-playbook playbook.yml
```
